#pragma once

#include "Defines.h"
#include "Gps.h"
#include <SD.h>
#include <VescUart.h>

struct SDLogger{
#ifndef MOCK_SD
	SDLogger(String fileName);

	struct Endl{};
	static Endl endl;
	struct File:private fs::File{
		File& precice(double val,int decilams);
		template<class T> File& operator<<(T val){
			fs::File::print(val);
			fs::File::print(";");
			return *this;
		}

		inline void flush() override{fs::File::flush();}
		File& operator<<(Endl);
		File& operator<<(decltype(VescUart::data)& data);
		File& operator<<(GPS::Pos& pos);

		void writeTimeHeader();
		void writeGpsHeader();
		void writeVescHeader();
	};

	void readConfig();


	void newFile(bool verbose=false);
	File& f();

private:
	fs::File current;
	String fileName;
	static bool sdInited;

#else
	inline SDLogger(String fileName){}
	struct Endl{};
	static Endl endl;
	struct File{
		template<class T> File& operator<<(T val){ return *this;}
		inline void flush(){}
		inline void writeTimeHeader(){}
		inline void writeGpsHeader(){}
		inline void writeVescHeader(){}
	};

	inline void newFile(){}
	inline File& f(){return current;}
private:
	File current;
#endif
};

