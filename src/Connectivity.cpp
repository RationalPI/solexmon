#include "Connectivity.h"

#include "Display.h"
#include "Defines.h"

#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <WiFi.h>
#include <time.h>

Connectivity::Connectivity()
	:mqtt_scalnsolex("scalnsolex.scalian.com",8883,"135f33f0-1791-4aae-ac05-f11510e57ca6","solex-link-service","PJcg$Qt%Y5C$Mjo!47^q")
	,mushu("http://93.22.23.145:8086","c1423687da9f62d4","rns","Ba1Qsx9prGXVdyyRYNvkPWfz5BHsbigr_vDiQ7oc_YCkWEllILW-8BSyVb1fsorP7SLIOX6CUCoI0_m39zH-6A==")
{
	display().console("Connectivity init");
	delay(1000);//waiting for Wifi hardware to be ready
	tryConnect(10000,true);
}

bool Connectivity::ok(){return WiFi.status() == WL_CONNECTED && mushu.validateConnection() && mqtt_scalnsolex.connected();}

bool Connectivity::tryConnect(uint16_t timeout, bool verbose) {
	if(WiFi.status() != WL_CONNECTED) {
		if(verbose){
			display().console("Connecting to WiFi:");
			display().console(wifi_ssid);
			display().console("With pass: ");
			display().console(wifi_pass);
		}
		WiFi.begin(wifi_ssid, wifi_pass);
	}
	for (int i = 0; i < timeout; ++i) {
		if(WiFi.status() == WL_CONNECTED){
			if(!mqtt_scalnsolex.connected()){
				if(verbose) display().console("Connecting to mqtt_scalnsolex");
				mqtt_scalnsolex.connect();
			}
			if(!timeSet){
				if(verbose) display().console("Setting time");
				configTime(0, 0, "pool.ntp.org");
				setenv("TZ","CET-1CEST,M3.5.0,M10.5.0/3",1);
				tzset();
				timeSet=true;
			}
			return true;
		}
		delay(1);
	}
	return false;
}

static String get_timestamp() {
	tm ti;
	getLocalTime(&ti);
	char timestamp[16];
	sprintf(timestamp, "%02d%02d%02d%02d%02d%02d%03ld",
			  ti.tm_year % 100, ti.tm_mon + 1, ti.tm_mday,
			  ti.tm_hour, ti.tm_min, ti.tm_sec, long(0));
	return String(timestamp, 15);
}

bool Connectivity::sendTelemetry(const Data& data){
	static StaticJsonDocument<400/*super good enought*/> json;

	if(tryConnect()){
		/*mqtt_scalnsolex*/{
			String json_string;
			json.clear();
			json["type"] = 1;
			json["solexId"] = "135f33f0-1791-4aae-ac05-f11510e57ca6";
			json["timestamp"] = timeSet?get_timestamp():"000000000000000";
			json["latitude"] = data.latitude;
			json["longitude"] = data.longitude;
			json["tension"] = data.inpVoltage;
			json["temperature"] = data.tempMotor;
			serializeJson(json, json_string);
			mqtt_scalnsolex.publish("input/135f33f0-1791-4aae-ac05-f11510e57ca6", json_string.c_str());
			mqtt_scalnsolex.loop();
		}
		/*mushu*/{
			Point p("Solex");

			p.addTag("solex","PurpleRocket");
			// p.setTime();
			p.addField("latitude", data.latitude, 6);
			p.addField("longitude", data.longitude, 6);
			p.addField("avgMotorCurrent", data.avgMotorCurrent);
			p.addField("avgInputCurrent", data.avgInputCurrent);
			p.addField("dutyCycleNow", data.dutyCycleNow);
			p.addField("rpm", data.rpm);
			p.addField("kmh", data.rpm*erpm2kmh);
			p.addField("inpVoltage", data.inpVoltage);
			p.addField("ampHours", data.ampHours);
			p.addField("ampHoursCharged", data.ampHoursCharged);
			p.addField("wattHours", data.wattHours);
			p.addField("wattHoursCharged", data.wattHoursCharged);
			p.addField("tempMosfet", data.tempMosfet);
			p.addField("tempMotor", data.tempMotor);
			p.addField("pidPos", data.pidPos);
			p.addField("tachometer", data.tachometer);
			p.addField("tachometerAbs", data.tachometerAbs);

			mushu.writePoint(p);
		}
		return true;
	}
	return false;
}

std::string Connectivity::getDate(){
	if(!timeSet) return "00/00-00:00:00";

	tm ti;
	getLocalTime(&ti);
	char timestamp[16];
	sprintf(timestamp, "%02d/%02d-%02d:%02d:%02d",
			  ti.tm_mday, ti.tm_mon + 1, ti.tm_hour, ti.tm_min, ti.tm_sec);
	return std::string(timestamp);
}

Connectivity::MQTT_Client::MQTT_Client(IPAddress ip, uint16_t port, std::string id, std::string user, std::string pass)
	:PubSubClient(wifi_client),ip(ip),port(port),id(id),username(user),password(pass)
{}

Connectivity::MQTT_Client::MQTT_Client(std::string domain, uint16_t port, std::string id, std::string user, std::string pass)
	:PubSubClient(wifi_client),domain(domain),port(port),id(id),username(user),password(pass)
{}

void Connectivity::MQTT_Client::connect(){
	if(domain.empty()){
		PubSubClient::setServer(ip,port);
	}else{
		PubSubClient::setServer(domain.c_str(),port);
	}
	PubSubClient::connect(id.c_str(),username.c_str(),password.c_str());
}
