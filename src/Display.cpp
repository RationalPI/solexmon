#include "Display.h"

#include "Defines.h"
#include <sstream>

Display &display(){
	static Display instance={};
	return instance;
}

Display::Display(){
	begin();
	setRotation(0);
}

void Display::draw(float soc, decltype(VescUart::data)& data, bool connectivityOK, std::string date, uint32_t satCount){
	if (!displayBusy()){
		startWrite();{
			fillScreen(TFT_BLACK);

			setTextColor(TFT_WHITE);
			if(soc<20)setTextColor(TFT_ORANGE);
			if(soc<10)setTextColor(TFT_RED);

			auto draw=[&](std::string s,float x,float y){
				drawString(s.c_str(),240*x,320*y);
			};
			auto drawBigLine=[&](std::string title,std::string val,float x,float y){
				setFont(&fonts::DejaVu72);
				setTextDatum(textdatum_t::top_centre);
				draw(val,x,y);

				setFont(&fonts::DejaVu12);
				setTextDatum(textdatum_t::top_centre);
				draw(title,x,y+0.2);
			};

			drawBigLine("mot (C)",std::to_string(data.tempMotor).substr(0,2),0.25,0);
			drawBigLine("fet (C)",std::to_string(data.tempMosfet).substr(0,2),0.75,0);

			//int(200/*magic*/*(81.6/*perimeter cm*//100000)*data.rpm/60)
			drawBigLine("Speed (km/h)",std::to_string(data.rpm*erpm2kmh).substr(0,2),0.5,0.3);
			drawBigLine("SOC (%)",std::to_string(soc).substr(0,2)+"%",0.5,0.6);

			setFont(&fonts::DejaVu12);

			setTextDatum(textdatum_t::bottom_left);
			draw("Voltage: "+std::to_string(data.inpVoltage).substr(0,5) +"V",0,0.95);

			setTextDatum(textdatum_t::bottom_left);
			draw(std::string("@")+(connectivityOK?"V":"X"),0,1);
			setTextDatum(textdatum_t::bottom_center);
			draw(date,0.5,1);
			setTextDatum(textdatum_t::bottom_right);
			draw(std::string("sat:")+std::to_string(satCount),1,1);

			// constexpr auto plotY=232;
			// drawFastHLine(0,plotY,240);
			// drawPixel(10,plotY+10);

		}endWrite();
	}
}

void Display::drawERROR(){
	if (!displayBusy()){
		startWrite();{
			fillScreen(TFT_BLACK);
			delay(10);
			fillScreen(TFT_WHITE);
			setTextColor(TFT_BLACK);
			setFont(&fonts::DejaVu72);
			setTextDatum(textdatum_t::middle_center);
			constexpr auto h=320/3;
			drawString("UART", 240/2, h);
			drawString("LOST", 240/2, h*2);
		}endWrite();
	}
}

void Display::blitSStr(std::stringstream &ss){
	if (!displayBusy()){
		startWrite();{
			fillScreen(TFT_BLACK);
			setTextColor(TFT_WHITE);
			setTextDatum(textdatum_t::top_left);
			setFont(&fonts::DejaVu24);

			std::string line;
			uint8_t lineIndex=0;
			while(std::getline(ss,line,';')){
				drawString(line.c_str(),0,30*lineIndex++);
			}
		}endWrite();
	}
}

void Display::console(std::string s){
	if(infoBuffer.size()>12){
		infoBuffer.pop_front();
	}
	infoBuffer.push_back(s);
	if (!displayBusy()){
		startWrite();{
			fillScreen(TFT_BLACK);
			setTextColor(TFT_WHITE);
			setTextDatum(textdatum_t::top_left);
			setFont(&fonts::DejaVu18);

			uint8_t lineIndex=0;
			for (auto& infoLine : infoBuffer) {
				drawString(infoLine.c_str(),0,24*lineIndex++);
			}
		}endWrite();
	}
}
