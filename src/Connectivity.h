#pragma once

#include <WString.h>
#include <PubSubClient.h>
#include <WiFiClient.h>
#include <InfluxDbClient.h>

struct Connectivity{
	Connectivity();
	bool ok();
	std::string getDate();

	struct Data {
		float latitude;
		float longitude;
		float avgMotorCurrent;
		float avgInputCurrent;
		float dutyCycleNow;
		float rpm;
		float inpVoltage;
		float ampHours;
		float ampHoursCharged;
		float wattHours;
		float wattHoursCharged;
		float tempMosfet;
		float tempMotor;
		float pidPos;
		long tachometer;
		long tachometerAbs;
	};
	bool sendTelemetry(const Data& data);
private:
	bool tryConnect(uint16_t timeout=1/*ms*/, bool verbose=false);

	// Wifi related
	const char* wifi_ssid = "TEAM_35";
	const char* wifi_pass = "RNS_2024";

	bool timeSet=false;

	struct MQTT_Client:PubSubClient{
		MQTT_Client(IPAddress ip, uint16_t port,std::string id, std::string user, std::string pass);
		MQTT_Client(std::string domain, uint16_t port,std::string id, std::string user, std::string pass);
		void connect();
	private:
		WiFiClient wifi_client;
		std::string id, username, password, domain;
		uint16_t port;
		IPAddress ip;
	};
	MQTT_Client mqtt_scalnsolex;
	InfluxDBClient mushu;
};


