#pragma once

#include <VescUart.h>
#include <M5GFX.h>
#include <list>

struct Display:M5GFX{
	void draw(float soc, decltype(VescUart::data)& data, bool connectivityOK, std::string date, uint32_t satCount);
	void drawERROR();
	void blitSStr(std::stringstream& ss);
	void console(std::string s);
private:
	Display();
	std::list<std::string> infoBuffer;
	friend Display& display();
};

Display& display();
