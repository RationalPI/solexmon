#include "Gps.h"

#include "Display.h"

#ifndef MOCK_GPS
GPS::GPS(){
	display().console("GPS init");
	gpsStream.begin(9600);
}

void GPS::synch(){
	if (millis() > 5000 && gps.charsProcessed() < 10){
		satCount_=-1;//No GPS detected: check wiring.
	}

	while (gpsStream.available() > 0){
		gps.encode(gpsStream.read());
	}
	if(gps.location.isValid() && gps.location.isUpdated()){
		pos.lat=gps.location.lat();
		pos.lon=gps.location.lng();
	}
	if(gps.satellites.isValid() && gps.satellites.isUpdated()){
		satCount_=gps.satellites.value();
	}

	gps.satellites.value();
}
#endif
