#pragma once

#include "Defines.h"
#include <VescUart.h>

#ifndef MOCK_UART

struct Vesc:private VescUart{
	Vesc();
	inline decltype(VescUart::data)& currentData(){return data;}
	inline bool queryData(int tryAttempts){
		for (int i = 0; i < tryAttempts; ++i) {
			if (getVescValues()){
				return true;
			}
		}
		return false;
	}
};

#else

struct Vesc{
	inline Vesc(){
		data.tempMotor=data.tempMosfet=data.inpVoltage=60;
	}
	inline decltype(VescUart::data)& currentData(){return data;}
	inline bool queryData(){
		data.inpVoltage-=0.005;
		if(data.inpVoltage<52)data.inpVoltage=60;
		data.tempMotor=data.tempMosfet=data.inpVoltage;
		data.rpm=((data.inpVoltage-52)/(60-50))*24000;
		return true;
	}
private:
	decltype(VescUart::data) data;
};

#endif
