#include "Vuart.h"
#include "Display.h"
#include "Connectivity.h"
#include "Gps.h"
#include "SDLoger.h"

void setup(){}

//! return state of charge in % for a 60v lipo pack
static double socFromVoltage(double v){
	// return 100*(1-(-0.047087258490149*v*v*v + 8.18405471742893*v*v - 475.483434172542*v +9236.01010938414)/19);
	auto ret=100*(1-(-0.047198299523295*v*v*v +8.17234644926679*v*v -473.01171537634*v +9154.8775490596)/20);
	return std::min(std::max(ret,0.),100.);
}

struct Cadenser{
	Cadenser(float frequency/*Hz*/):dt(1000000/frequency){}
	void tick(){
		int64_t current;
		while (true){
			current=esp_timer_get_time();
			if(current>nextTarget) break;
		}
		nextTarget=current+dt;
	}
	//! force the cadenser to trigger imediatly
	void force(){
		nextTarget-=dt;
	}
private:
	int64_t nextTarget=0;//!< in microseconds
	int64_t dt;//!< in microseconds
};

struct Counter{
	Counter(uint64_t tickInterval,std::function<void()> f):tickInterval(tickInterval),f(f){}
	void tick(){
		current++;
		if(current>=tickInterval){
			current=0;
			f();
		}
	}
	//! force the Counter to trigger imediatly
	void force(){
		current=tickInterval;
	}
private:
	uint64_t tickInterval;
	std::function<void()> f;
	uint64_t current=0;
};

constexpr auto LoopFrequency=30;

struct Memento{
	Memento(){
		for (int i = 0; i < 10*LoopFrequency/*10sec buffer*/; ++i) {
			buffer.push_back(0);
		}
	}
	void remember(float v){
		buffer.push_back(v);
		buffer.pop_front();
	}
	float maxLast10Sec(){
		return *std::max_element(buffer.begin(),buffer.end());
	}
private:
	std::list<float> buffer;
};

void loop(){
#ifdef MOCK_SD
	display().info("/!\\ MOCK SD /!\\");
#endif
#ifdef MOCK_UART
	display().info("/!\\ MOCK UART /!\\");
#endif
#ifdef MOCK_GPS
	display().info("/!\\ MOCK GPS /!\\");
#endif
	Connectivity connection;
	GPS gps;
	SDLogger log("VescLog");
	log.f().writeTimeHeader();
	log.f().writeGpsHeader();
	log.f().writeVescHeader();

	Memento vMemento;
	Vesc vesc;

	Counter updateDisplay(2*LoopFrequency/*2sec*/,[&](){
		display().draw(socFromVoltage(vMemento.maxLast10Sec()),vesc.currentData(),connection.ok(),connection.getDate(),gps.satCount());
		log.f().flush();
	});

	Counter updateCloud(1*LoopFrequency/*1sec*/,[&](){
		Connectivity::Data data;{
			data.latitude=gps.currentPos().lat;
			data.longitude=gps.currentPos().lon;
			data.avgMotorCurrent=vesc.currentData().avgMotorCurrent;
			data.avgInputCurrent=vesc.currentData().avgInputCurrent;
			data.dutyCycleNow=vesc.currentData().dutyCycleNow;
			data.rpm=vesc.currentData().rpm;
			data.inpVoltage=vesc.currentData().inpVoltage;
			data.ampHours=vesc.currentData().ampHours;
			data.ampHoursCharged=vesc.currentData().ampHoursCharged;
			data.wattHours=vesc.currentData().wattHours;
			data.wattHoursCharged=vesc.currentData().wattHoursCharged;
			data.tempMosfet=vesc.currentData().tempMosfet;
			data.tempMotor=vesc.currentData().tempMotor;
			data.pidPos=vesc.currentData().pidPos;
			data.tachometer=vesc.currentData().tachometer;
			data.tachometerAbs=vesc.currentData().tachometerAbs;
		}
		connection.sendTelemetry(data);
	});

	Counter renewLogFile(10*60*LoopFrequency/*10min*/,[&](){
		log.newFile();
		log.f().writeTimeHeader();
		log.f().writeGpsHeader();
		log.f().writeVescHeader();
	});

	display().console("start loop");
	Cadenser cadenser(LoopFrequency);
	while (true) {
		cadenser.tick();
		gps.synch();

		if ( vesc.queryData(5) ) {
			vMemento.remember(vesc.currentData().inpVoltage);
			log.f() << esp_timer_get_time() << connection.getDate().c_str() << gps.currentPos() << vesc.currentData() << SDLogger::endl;
			updateDisplay.tick();
			renewLogFile.tick();
			updateCloud.tick();
		}else{
			cadenser.force();
			updateDisplay.force();
			display().drawERROR();
		}
	}
}
