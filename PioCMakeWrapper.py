import os
import sys
from filecmp import cmp

target_name = sys.argv[1]

print("==== PioCMakeWrapper::1/3: Generating temporary qtcreator project files")
sys.stdout.flush()

os.system("pio project init --ide qtcreator")

print("==== PioCMakeWrapper::2/3: generating pio.cmake")
sys.stdout.flush()

with open(f"{target_name}.tmp.cmake", "w") as pioCMake:
	pioCMake.write(f'add_custom_target(pio_build_{target_name} COMMAND pio run WORKING_DIRECTORY ${{CMAKE_CURRENT_LIST_DIR}} USES_TERMINAL)\n')
	pioCMake.write(f'add_custom_target(pio_build_and_upload_{target_name} COMMAND pio run --target upload WORKING_DIRECTORY ${{CMAKE_CURRENT_LIST_DIR}} USES_TERMINAL)\n\n')

	with open("platformio.config") as config:
		for line in config:
			pioCMake.write(f'add_compile_definitions({"=".join((line[8:-1]).split(" "))})\n')

	with open("platformio.files") as files:
		sources = []
		miscs = ["PioCMakeWrapper.py\n"]
		for f in files:
			if ".cpp" in f or ".h" in f:
				sources.append(f)
			elif "Makefile" not in f:
				miscs.append(f)

		pioCMake.write(f'\nadd_custom_target(Utils SOURCES\n	{"	".join(miscs)})\n')
		pioCMake.write(f'\n################\nadd_executable({target_name}\n	{"	".join(sources)})\n')

	with open("platformio.cflags") as cflags:
		for line in cflags:
			cf = line[:-1]
	with open("platformio.cxxflags") as cxxflags:
		for line in cxxflags:
			cxxf = line[:-1]
	pioCMake.write(f"target_compile_options({target_name} PRIVATE\n	$<$<COMPILE_LANGUAGE:CXX>:{cxxf}>\n	$<$<COMPILE_LANGUAGE:C>:{cf}>\n)\n")

	with open("platformio.includes") as includes:
		pioCMake.write(f'target_include_directories({target_name} PRIVATE\n	{"	".join(includes.readlines())})\n')

print("==== PioCMakeWrapper::3/3: Cleaning temporary qtcreator project files")
sys.stdout.flush()

if not cmp(f"{target_name}.tmp.cmake",f"{target_name}.cmake"):
	os.replace(f"{target_name}.tmp.cmake",f"{target_name}.cmake")
else:
	os.remove(f"{target_name}.tmp.cmake")

os.remove("platformio.creator")
os.remove("platformio.files")
os.remove("platformio.cflags")
os.remove("platformio.cxxflags")
os.remove("platformio.config")
os.remove("platformio.includes")
os.remove("Makefile")

